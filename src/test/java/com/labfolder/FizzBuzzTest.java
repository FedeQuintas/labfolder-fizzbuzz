package com.labfolder;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import com.labfolder.domain.FizzBuzzGame;
import com.labfolder.printer.FizzBuzzPrinter;
import com.labfolder.validator.FizzBuzzValidator;

public class FizzBuzzTest {

	private static final String BUZZ = "Buzz";
	private static final String FIZZ = "Fizz";
	private String exceptionMessage = "Input must be a positive number";
	private FizzBuzzPrinter fizzBuzzPrinterMock;
	private FizzBuzzGame fizzBuzzGame;
	private ArgumentCaptor<String> argument;

	@Before
	public void before() {

		fizzBuzzPrinterMock = mock(FizzBuzzPrinter.class);
		fizzBuzzGame = new FizzBuzzGame(fizzBuzzPrinterMock,
				new FizzBuzzValidator());
		argument = ArgumentCaptor.forClass(String.class);
	}

	@Test
	public void whenCountFromOneToOneThenPrintsOnlyOne() {

		fizzBuzzGame.play(1);

		verify(fizzBuzzPrinterMock).print(argument.capture());

		String expectedPrintableMessage = String.valueOf(1);

		Assert.assertEquals(expectedPrintableMessage, argument.getValue());

	}

	@Test
	public void whenInputIsZeroThenExceptionIsThrown() {

		try {
			fizzBuzzGame.play(0);
			fail();
		} catch (IllegalArgumentException exception) {
			Assert.assertEquals(exceptionMessage, exception.getMessage());
		}
	}

	@Test
	public void whenInputIsNegativeThenExceptionIsThrown() {

		try {
			fizzBuzzGame.play(-1);
			fail();
		} catch (IllegalArgumentException exception) {
			Assert.assertEquals(exceptionMessage, exception.getMessage());
		}
	}

	@Test
	public void whenInputIsNullThenExceptionIsThrown() {

		try {
			fizzBuzzGame.play(-1);
			fail();
		} catch (IllegalArgumentException exception) {
			Assert.assertEquals(exceptionMessage, exception.getMessage());
		}
	}

	@Test
	public void whenCountFromOneToTwoThenPrintsTwo() {

		fizzBuzzGame.play(2);

		verify(fizzBuzzPrinterMock, times(2)).print(argument.capture());

		String secondExpectedPrintableMessage = String.valueOf(2);

		Assert.assertEquals(secondExpectedPrintableMessage, argument.getValue());

	}

	@Test
	public void whenCountFromOneToThreeThenPrintsFizz() {

		fizzBuzzGame.play(3);

		verify(fizzBuzzPrinterMock, times(3)).print(argument.capture());

		String expectedPrintableMessage = String.valueOf(FIZZ);

		Assert.assertEquals(expectedPrintableMessage, argument.getValue());

	}

	@Test
	public void whenCountFromOneToFiveThenPrintsBuzz() {

		fizzBuzzGame.play(5);

		verify(fizzBuzzPrinterMock, times(5)).print(argument.capture());

		String expectedPrintableMessage = String.valueOf(BUZZ);

		Assert.assertEquals(expectedPrintableMessage, argument.getValue());

	}

	@Test
	public void whenCountFromOneToFifteenThenPrintsFizzBuzz() {

		fizzBuzzGame.play(15);

		verify(fizzBuzzPrinterMock, times(15)).print(argument.capture());

		String expectedPrintableMessage = new StringBuilder().append(FIZZ)
				.append(BUZZ).toString();

		Assert.assertEquals(expectedPrintableMessage, argument.getValue());

	}

	@Test
	public void whenCountFromOneToEighteenThenPrintsBuzz() {

		fizzBuzzGame.play(18);

		verify(fizzBuzzPrinterMock, times(18)).print(argument.capture());

		String expectedPrintableMessage = String.valueOf(FIZZ);

		Assert.assertEquals(expectedPrintableMessage, argument.getValue());

	}

	@Test
	public void whenCountFromOneToTwentyThenPrintsBuzz() {

		fizzBuzzGame.play(20);

		verify(fizzBuzzPrinterMock, times(20)).print(argument.capture());

		String expectedPrintableMessage = String.valueOf(BUZZ);

		Assert.assertEquals(expectedPrintableMessage, argument.getValue());

	}

	@Test
	public void whenCountFromOneToThirtyThenPrintsFizzBuzz() {

		fizzBuzzGame.play(30);

		verify(fizzBuzzPrinterMock, times(30)).print(argument.capture());

		String expectedPrintableMessage = new StringBuilder().append(FIZZ)
				.append(BUZZ).toString();

		Assert.assertEquals(expectedPrintableMessage, argument.getValue());

	}

}
