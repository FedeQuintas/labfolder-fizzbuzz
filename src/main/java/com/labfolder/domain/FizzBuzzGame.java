package com.labfolder.domain;

import com.labfolder.chain.DivisibleByNumberChain;
import com.labfolder.chain.DivisibleByThreeAndFiveChain;
import com.labfolder.chain.DivisibleChain;
import com.labfolder.chain.NotDivisibleChain;
import com.labfolder.printer.FizzBuzzPrinter;
import com.labfolder.validator.FizzBuzzValidator;

public class FizzBuzzGame {

	private static final String FIZZ = "Fizz";
	private static final String BUZZ = "Buzz";
	private FizzBuzzPrinter fizzBuzzPrinter;
	private FizzBuzzValidator fizzBuzzValidator;
	private DivisibleChain first;

	public FizzBuzzGame(FizzBuzzPrinter fizzBuzzPrinter,
			FizzBuzzValidator fizzBuzzValidator) {
		this.fizzBuzzPrinter = fizzBuzzPrinter;
		this.fizzBuzzValidator = fizzBuzzValidator;
		createChain();
	}

	private void createChain() {

		first = new DivisibleByThreeAndFiveChain();

		DivisibleChain divisibleByFiveChain = new DivisibleByNumberChain(5,
				BUZZ);

		first.next(divisibleByFiveChain);

		DivisibleChain divisibleByThreeChain = new DivisibleByNumberChain(3,
				FIZZ);

		divisibleByFiveChain.next(divisibleByThreeChain);

		DivisibleChain notDivisibleChain = new NotDivisibleChain();

		divisibleByThreeChain.next(notDivisibleChain);

	}

	public void play(int topNumber) {
		validateInput(topNumber);
		for (int i = 1; i <= topNumber; i++) {
			String message = first.generateFizzBuzzMessage(i);
			fizzBuzzPrinter.print(message);
		}
	}

	private void validateInput(int topNumber) {
		fizzBuzzValidator.validate(topNumber);
	}
}
