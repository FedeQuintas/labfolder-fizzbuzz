package com.labfolder.main;

import com.labfolder.domain.FizzBuzzGame;
import com.labfolder.printer.FizzBuzzPrinter;
import com.labfolder.validator.FizzBuzzValidator;

public class Main {

	public static void main(String[] args) {
		FizzBuzzPrinter fizzBuzzPrinter = new FizzBuzzPrinter();
		FizzBuzzValidator fizzBuzzValidator = new FizzBuzzValidator();
		FizzBuzzGame fizzBuzzGame = new FizzBuzzGame(fizzBuzzPrinter,
				fizzBuzzValidator);
		fizzBuzzGame.play(Integer.parseInt(args[0]));
		
	}

}
