package com.labfolder.validator;

public class FizzBuzzValidator {

	private static final String INPUT_MUST_BE_POSITIVE = "Input must be a positive number";

	public void validate(Integer topNumber) {
		if (topNumber == null || topNumber < 1) {
			throw new IllegalArgumentException(INPUT_MUST_BE_POSITIVE);
		}
	}
}
