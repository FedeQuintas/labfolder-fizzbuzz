package com.labfolder.chain;

public abstract class DivisibleChain {

	public abstract String generateFizzBuzzMessage(int dividend);

	public boolean isDivisibleBy(int dividend, int divider) {
		return dividend % divider == 0;
	}

	public abstract void next(DivisibleChain divisibleByFiveChain);

}
