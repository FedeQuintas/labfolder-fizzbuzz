package com.labfolder.chain;

public class DivisibleByNumberChain extends DivisibleChain {

	private DivisibleChain next;
	private String message;
	private int divisor;

	public DivisibleByNumberChain(int divisor, String message) {
		this.message = message;
		this.divisor = divisor;
	}

	@Override
	public String generateFizzBuzzMessage(int dividend) {
		if (!isDivisibleBy(dividend, divisor)) {
			return next.generateFizzBuzzMessage(dividend);
		}
		return message;

	}

	public void next(DivisibleChain divisibleChain) {
		this.next = divisibleChain;
	}

}
