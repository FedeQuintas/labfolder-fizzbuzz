package com.labfolder.chain;

public class DivisibleByThreeAndFiveChain extends DivisibleChain {

	private static final String FIZZ_BUZZ = "FizzBuzz";
	private DivisibleChain next;

	@Override
	public String generateFizzBuzzMessage(int dividend) {
		if (!isDivisibleBy(dividend, 3) || !isDivisibleBy(dividend, 5)) {
			return next.generateFizzBuzzMessage(dividend);
		}
		return FIZZ_BUZZ;
	}

	public void next(DivisibleChain divisibleChain) {
		this.next = divisibleChain;
	}

}
