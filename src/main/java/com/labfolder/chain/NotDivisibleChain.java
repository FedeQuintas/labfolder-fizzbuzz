package com.labfolder.chain;

public class NotDivisibleChain extends DivisibleChain {

	@Override
	public String generateFizzBuzzMessage(int dividend) {
		return String.valueOf(dividend);
	}

	@Override
	public void next(DivisibleChain divisibleByFiveChain) {

	}

}
